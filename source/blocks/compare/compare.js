//=========================================================================
//=========================================================================
//
// Блок сравнения .compare

// Инициализация плагина перетаскивания
var dragger = new Draggabilly( '.compare__dragger', {
	axis: 'x',
	containment: '.compare',
	handle: '.compare__dragger--circle'
});

/*
Функция для замены изображений в блоке
======================================
	— beforeSrc: изображение До
	— afterSrc: изображение После
======================================
*/ 
function setCompareImages(beforeSrc, afterSrc) {

	// Получаем блоки с изображениями
	var
		imageBefore = $(".compare__image.image-before .image-content"),
		imageAfter  = $(".compare__image.image-after .image-content");

	// Устанавливаем ширину по ширине экрана
	imageBefore.width( $(window).width() );
	imageAfter.width( $(window).width() );

	// Анимируем фейдом и устанавливаем на фон изображение До
	imageBefore
		.animate({
			opacity: 0
		}, 400, function() {
			$(this)
				.css("background", "transparent url('" + beforeSrc + "') no-repeat center")
				.animate({opacity: 1}, 400);
		});

	// Анимируем фейдом и устанавливаем на фон изображение После
	imageAfter
		.animate({
			opacity: 0
		}, 400, function() {
			$(this)
				.css("background", "transparent url('" + afterSrc + "') no-repeat center")
				.animate({opacity: 1}, 400);
		});

}


function setImageWidth() {

	// Получаем контейнер блока с изображением После
	var imageAfter = $(".compare__image.image-after");

	// Вычисляем новую ширину (ширина окна минус сумма сдвига ползунка с левого края и половины его ширины)
	var newWidth = $(window).width() - ($(".compare__dragger--circle").offset().left + $(".compare__dragger--circle").width() / 2)

	// Устанавливаем ширину контейнера согласно вычисленной ширине
	imageAfter.width( newWidth + 64 );

}


// ФИксим ширину при загрузке страницы
setImageWidth();


// Событие, вызываемое при перетаскивании ползунка
dragger.on( 'dragMove', function( event, pointer, moveVector ) {

	setImageWidth();

});

//
//=========================================================================
//=========================================================================