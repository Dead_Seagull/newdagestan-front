//=========================================================================
//=========================================================================
//
// Блок переключалок между локациями

/*
Функция для установки фоновых изображений в переключалки
======================================
	— prevImage: изображение для предыдущей локации
	— nextImage: изображение для следующей локации
======================================
*/ 
function setArrowsImages(prevImage, nextImage) {

	// Если первый параметр не определен,
	// значит текущая локация первая и прячем переключалку
	if (prevImage === undefined)
		$(".item-arrow.arrow-prev")
			.addClass('disabled')
			.animate({opacity: 0}, 400);

	// Иначе удаляем класс неактивности, фейдим, меняем фоновую картинку и показываем обратно
	else {
		$(".item-arrow.arrow-prev")
			.removeClass('disabled')
			.animate({
				opacity: 0
			}, 400, function() {
				$(this)
					.css("background", "#000 url('" + prevImage + "') no-repeat center")
					.animate({opacity: 1}, 400);
			});
	}


	// Если второй параметр не определен,
	// значит текущая локация последняя и прячем переключалку
	if (nextImage === undefined)
		$(".item-arrow.arrow-next")
			.addClass('disabled')
			.animate({opacity: 0}, 400);

	// Иначе удаляем класс неактивности, фейдим, меняем фоновую картинку и показываем обратно
	else {
		$(".item-arrow.arrow-next")
			.removeClass('disabled')
			.animate({
				opacity: 0
			}, 400, function() {
				$(this)
					.css("background", "#000 url('" + nextImage + "') no-repeat center")
					.animate({opacity: 1}, 400);
			});
	}

}


// Переходим на следующую локацию
$(".item-arrow.arrow-next").on('click', function() {
	
	app.showNextLocation();

});


// Переходим на предыдущую локацию
$(".item-arrow.arrow-prev").on('click', function() {
	
	app.showPreviousLocation();

});

//
///=========================================================================
//==========================================================================