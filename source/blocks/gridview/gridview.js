//=========================================================================
//=========================================================================
//
// Блок просмотра всех локаций

$(document).ready(function() {
	fillGrid(app.data);
	initGridviewSlider();
});

// Функция запуска работы со слайдером в сетке (необходима полная загрузка Dom)
function initGridviewSlider() {

	// Пихаем картинки в фон, чтобы не было пакостей при разнобое в пропорциях изображений
	$('.gridview__item-image').each(function() {

		// Берем сорс
		var src = $(this).find('img').attr('src');

		// Ремувим пикчу
		$(this).find('img').remove();

		// Ставим фон с указанным сорсом 
		$(this).css("background", "#000 url('" + src + "') no-repeat center")

	});

	// После чего запускаем плагин слайдера с панелями
	$("#gridSlider").flickity({
		cellSelector: '.gridview__slide',
		wrapAround: true,
		prevNextButtons: false,
		setGallerySize: false
	});
	
	// Получаем переключалку
	var gridSwitcher = $(".gridview__switcher");

	// При клике перелючаем активный класс на параключалке и просмотре сетки
	gridSwitcher.on('click', function() {

		$(this).toggleClass('active');
		$(".gridview").toggleClass('gridview--active');

	});



	$(".gridview__item").on('click', function() {

		var index = $(this).data("index");

		app.setLocationInfo( index );

		$(".gridview").removeClass('gridview--active');
		$(".gridview__switcher").removeClass('active');

	});
}


// Устанавливаем счетчик слайдов в вновое значение
function setCounter(string) {

	$(".gridview__counter").text(string);

}


// Заполняем сетку блоками
function fillGrid(data) {

	// Получаем количество нужных слайдов
	var slidesAmount = Math.floor( data.length / 8 ) + 1;
	
	// Для каждого слайда
	for (var i = 1; i <= slidesAmount; i++) {

		// Создаем элемент
		var newSlide = $("<div class='gridview__slide'></div>");

		// В цикле на каждые 8 элементов
		for (var j = (i*8 - 8); j <= (i*8 - 1); j++) {

			// Проверяем, чтобы не выйти за длину массива
			if (j > data.length - 1)
				return;


			// Получаем строки с названием и фотографией локации
			var title = data[j].title;
			var image = data[j].racurses[0].imageAfter;

			// Составляем html-строку
			var htmlString = 
			'<div class="gridview__item" data-index="' + j + '">' +
	            '<div class="gridview__item-title">' + title + '</div>' +
	            '<div class="gridview__item-image"><img src="' + image + '" alt=""/></div>' +
          	'</div>';

          	// Создаем элемент
          	var gridElement = $(htmlString);

          	// Вставляем элемент в слайд
          	newSlide.append(gridElement);

          	// И вставляем слайд в слайдер
			$(".gridview__slider").append(newSlide);
		}
	}
}
//
//=========================================================================
//=========================================================================