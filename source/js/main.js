//=========================================================================
//=========================================================================
//
// Главный скрипт

// Данные - как образец
var data =
[
	{
		title: "Памяник русской учительнице",
		location: "г. Махачкала, пр. Петра I",
		date: "2001 / 2015",
		link: "#link1",
		racurses: [
			{
				imageBefore: 'img/photo/big1.png',
				imageAfter: 'img/photo/big2.png'
			},

			{
				imageBefore: 'img/photo/big1.png',
				imageAfter: 'img/photo/big2.png'
			}
		]
	},
	
	{
		title: "Привокзальная площадь",
		location: "г. Махачкала, ул. Азизова",
		date: "2001 / 2015",
		link: "#link2",
		racurses: [
			{
				imageBefore: 'img/photo/2.jpg',
				imageAfter: 'img/photo/2.jpg'
			}
		]
	},
	
	{
		title: "Джума мечеть",
		location: "г. Махачкала, ул. Азизова",
		date: "2001 / 2015",
		link: "#link3",
		racurses: [
			{
				imageBefore: 'img/photo/3.jpg',
				imageAfter: 'img/photo/3.jpg'
			}
		]
	},

	{
		title: "Какая-то высотка в америке",
		location: "г. Махачкала, ул. Азизова",
		date: "2001 / 2015",
		link: "#link4",
		racurses: [
			{
				imageBefore: 'img/photo/4.jpg',
				imageAfter: 'img/photo/4.jpg'
			}
		]
	}
];


function App(options) {

	this.options = options || {};
	this.currentItem = 0;

	// Устанавливаем UI согласно текущей локации
	this.setLocationInfo = function(index) {

		var place = this.data[index]

		this.currentItem = index;

		console.log('Init location: ' + place.title);

		// Установка изображений сравнения
		var
			imageBefore = place.racurses[0].imageBefore,
			imageAfter  = place.racurses[0].imageAfter;
		
		setCompareImages(imageBefore, imageAfter);

		// Установка данных о локации
		setInfoData(place.title, place.location, place.date, place.link);

		// Установка ракурсов
		initRacurses(place.racurses);

		// Установка изображений на кнопки следующей и предыдущей локации
		var
			nextArrowImage = this.getArrowImages().next,
			prevArrowImage = this.getArrowImages().prev;

		setArrowsImages(prevArrowImage, nextArrowImage);

		setCounter( index + 1 + " / " + this.amount );

	}


	// Инициализация приложения с массивом данных
	this.init = function(data) {

		if (data === undefined || data.length === 0) {
			console.log('Нет данных');
			return;
		}

		this.data   = data;
		this.amount = this.data.length;

		this.setLocationInfo( this.currentItem );

		setCounter( "1 / " + this.amount );
	}


	// Переключение на следующую локацию
	this.showNextLocation = function() {
		console.log('Switching to next location...');

		this.currentItem !== this.amount - 1 ? this.currentItem++ : '';

		this.setLocationInfo( this.currentItem );

		var selectedNumber = this.currentItem + 1;
		setCounter( selectedNumber + " / " + this.amount );
	}


	// Переключение на предыдущую локацию
	this.showPreviousLocation = function() {
		console.log('Switching to previous location...');
		
		this.currentItem > 0 ? this.currentItem-- : '';

		this.setLocationInfo( this.currentItem );

		var selectedNumber = this.currentItem + 1;
		setCounter( selectedNumber + " / " + this.amount );
	}


	// Получение изображений для стрелок
	this.getArrowImages = function() {

		var imageData = {};

		// Установка изображения предыдущей локации
		if (this.currentItem === 0)
			imageData.prev = undefined;
		
		else
			imageData.prev = this.data[ this.currentItem - 1 ].racurses[0].imageAfter;


		// Установка изображения следующей локации
		if (this.currentItem === this.amount - 1)
			imageData.next = undefined;

		else
			imageData.next = this.data[ this.currentItem + 1 ].racurses[0].imageAfter;


		return imageData;
	}

}


// Создаем объект класса APP
var app = new App();


// Инициализация приложения
app.init(data);

//
//=========================================================================
//=========================================================================