// Npm-packages
const
  del     = require('del'),
  gulp    = require('gulp'),
  path    = require('path'),
  jade    = require('gulp-jade'),
  stylus  = require('gulp-stylus'),
  concat  = require('gulp-concat'),
  plumber = require('gulp-plumber'),
  sourcemaps = require('gulp-sourcemaps'),
  sync    = require('browser-sync').create();

// PostCSS
const
  postcss      = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  stylefmt     = require('stylefmt'),
  sorting      = require('postcss-sorting');

// Path vars
const
  devPath = './dev/',
  cssPath = path.join(devPath, 'css/'),
  jsPath  = path.join(devPath, 'js/');



// MAIN THREAD
// Styles
gulp.task('styles', () => {

  return gulp
    .src('./source/styles/*.styl')
    .pipe( plumber() )
    .pipe( stylus()  )
    .pipe( postcss( [ autoprefixer, sorting({"sort-order": "csscomb"}) ] ) )
    .pipe( gulp.dest(cssPath) );
});


// Pages
gulp.task('pages', () => {

  return gulp
    .src('./source/pages/*.jade')
    .pipe( jade({pretty: true}) )
    .pipe( gulp.dest(devPath)   );

});


// Scripts
gulp.task('scripts', () => {

  return gulp
    .src(['./source/js/main.js', './source/blocks/**/*.js'])
    .pipe( sourcemaps.init() )
    .pipe( concat('scripts.js') )
    .pipe( sourcemaps.write('.') )
    .pipe( gulp.dest(jsPath) );

});


// Files
gulp.task('assets', () => {

  return gulp
    .src('./source/assets/**/*',
      {since: gulp.lastRun('assets')})
    .pipe( gulp.dest(devPath) );

});


// Server
gulp.task('serve', () => {

  sync
  .init({
    server: './dev'
  });

  sync
    .watch('./dev/**/*')
    .on('change', sync.reload);

});


// Clean
gulp.task('clean', () => {

  return del('./dev');

});


// Watch
gulp.task('watch', () => {

  gulp.watch('source/**/*.styl', gulp.series('styles' ));
  gulp.watch('source/**/*.jade', gulp.series('pages'  ));
  gulp.watch('source/**/*.js'  , gulp.series('scripts'));

});


// Dev build
gulp.task('dev', gulp.series('styles', 'pages', 'scripts', 'assets'));


// Default task
gulp.task('default', gulp.series('clean', 'dev', gulp.parallel('watch', 'serve')) );